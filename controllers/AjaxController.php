<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Car;
use app\models\Brand;
use app\models\Region;
use app\models\Model;
use app\models\Complectation;
use app\models\Request;
use app\models\User;

use yii\helpers\ArrayHelper;

class AjaxController extends GlobalController
{  
    public function actionCompleterequest()
    {
        $response = [];
        $response['success'] = false;
        $response['message'] = '';
        if (empty(Yii::$app->session->get('admin'))){
            $response['message'] = 'Не авторизован';
            echo json_encode($response);
            die();
        }
        $grade_id = Yii::$app->request->post('grade_id');
        $request_id = Yii::$app->request->post('request_id');
        $request = Request::find()
                ->where([
                    'id' => $request_id,
                    'completed' => 0,
                    'manager_id' => Yii::$app->session->get('id')
                ])
                ->one();
        $request->solution_id = 1;
        $request->completed = 1;
        $request->completed_at = (new \DateTime())->format("Y-m-d H:i:s");
        $request->grade_id = $grade_id;
        if (!$request->save()){
            $user = User::findOne(Yii::$app->session->get('id'));
            $user->requests_amount = $user->requests_amount-1;
            $user->save();
            $response['message'] = $request->getErrors();
            echo json_encode($response);
            die();
        }
        $response['success'] = true;
        echo json_encode($response);
        die();
    }
    
    public function actionModels()
    {
        $brand_id = $_GET['brand_id']; 
        $models = Model::find()
                ->orderBy(['name'=> SORT_ASC])
                ->where([
                    'status' => 1,
                    'brand_id' => $brand_id
                ])
                ->all();
        $options = "";
        foreach ($models as $key => $model) {
            $options .= '<option data-img="'.$model->img_path.'" value="'.$model->id.'">'.$model->name.'</option>';
        }   
        echo $options;
    }
    
    public function actionComplectations()
    {
        $model_id = $_GET['model_id']; 
        $complectations = Complectation::find()
                ->orderBy(['name'=> SORT_ASC])
                ->where([
                    'status' => 1,
                    'model_id' => $model_id
                ])
                ->all();
        $options = "";
        foreach ($complectations as $key => $complectation) {
            $options .= '<option data-price="'.$complectation->price.'" value="'.$complectation->id.'">'.$complectation->name.'</option>';
        }   
        echo $options;
    }
    
    public function actionCalculate(){
        $complectation_id = $_POST['complectation_id'];
        $first_payment = $_POST['first_payment'];
        $years = $_POST['years'];
        
        $response = [];
        $response['success'] = false;
        $response['message'] = '';
        
        if ($years<6 || $years>84){
            $response['code'] = 1;
            $response['message'] = "Некорректный срок кредитования";
            echo json_encode($response);
            die;
        }
        
        $complectation = Complectation::findOne($complectation_id);
        
        if (empty($complectation)){
            $response['code'] = 2;
            $response['message'] = "Комплектация не найдена";
            echo json_encode($response);
            die;
        }
        
        $everymonth_payment = Request::calculatePayment($complectation->price, $first_payment, $years);
        $response['success'] = true;
        $response['result'] = $everymonth_payment;
        echo json_encode($response);
        die;
    }
    
    public function actionAddrequest(){
        $complectation_id = $_POST['complectation'];
        $price = $_POST['price'];
        $fio = $_POST['fio'];
        $phone = $_POST['phone'];
        $age = $_POST['age'];
        $region_id = $_POST['region'];
        $first_payment = $_POST['first_payment'];
        $first_payment_percent = $_POST['first_payment_percent'];
        $years = $_POST['years'];
        $tradein = $_POST['tradein'];
        
        $response = [];
        $response['success'] = false;
        $response['message'] = '';
        
        if ($years<6 || $years>84){
            $response['code'] = 1;
            $response['message'] = "Некорректный срок кредитования";
            echo json_encode($response);
            die;
        }
        
        $complectation = Complectation::findOne($complectation_id);
        
        if (empty($complectation)){
            $response['code'] = 2;
            $response['message'] = "Комплектация не найдена";
            echo json_encode($response);
            die;
        }
        
        $everymonth_payment = Request::calculatePayment($complectation->price, $first_payment, $years);
        
        $alf = [];
        foreach(range('A','Z') as $i)
            $alf[] = $i;
        
        $request = new Request();
        $request->number = $alf[array_rand($alf)].(string)rand(0,9).(string)rand(0,9).(string)rand(0,9).(string)rand(0,9);
        $request->car_id = NULL;
        $request->complectation_id = $complectation_id;
        $request->region_id = $region_id;
        $request->solution_id = 1;
        $request->price = $complectation->price;
        $request->completed = 0;
        $request->fio = $fio;
        $request->phone = $phone;
        $request->age = $age;
        $request->first_payment = $first_payment;
        $request->first_payment_percent = $first_payment_percent;
        $request->every_month_payment = $everymonth_payment;
        $request->years = $years;
        $request->tradeIn_car = $tradein;
        $request->fake = 0;
        $request->status = 1;
        $user_to_request = User::find()->orderBy(['requests_amount'=> SORT_ASC])->one();
        $request->manager_id = $user_to_request->id;
        if (!$request->save()){
            $response['success'] = false;
            $response['message'] = $request->getErrors();
            echo json_encode($response);
            die;
        }
        $user_to_request->requests_amount = $user_to_request->requests_amount+1;
        $user_to_request->save();
        $region = Region::findOne($request->region_id);
        $complectation = Complectation::findOne($request->complectation_id);
        $model = Model::findOne($complectation->model_id);
        $brand = Brand::findOne($model->brand_id);
        $emails = Yii::$app->params['adminEmails'];
        foreach ($emails as $email) {
            $mes_result = Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['notificationEmail'])
                ->setTo($email)
                ->setSubject('Элекс Полюс – Заявка на кредит №'.$request->id)
                ->setHtmlBody(
                        '<b>АВТОМОБИЛЬ</b><br>'.
                        'Марка: '.$brand->name.'<br>'.
                        'Модель: '.$model->name.'<br>'.
                        'Компл.: '.$complectation->name.'<br>'.
                        'Цена: '.number_format(($request->price), 0, '.', ' ').' р.<br><br>'.
                        '<b>КРЕДИТ</b><br>'.
                        'Взнос: '.number_format(($request->first_payment), 0, '.', ' ').' р. ('.$request->first_payment_percent.'%)<br>'.
                        'Сумма: '.number_format(($request->price), 0, '.', ' ').' р.<br>'.
                        'Срок: '.(int)$request->years.' месяцев<br>'.
                        'Платёж: '.number_format(($request->every_month_payment), 0, '.', ' ').' р.<br><br>'.
                        '<b>КОНТАКТЫ</b><br>'.
                        'ФИО: '.$request->fio.'<br>'.
                        'Возраст: '.$request->age.'<br>'.
                        'Регистрация: '.$region->name.'<br>'.
                        'Телефон: '.$request->phone.'<br><br>'.
                        'Trade-In: '.($request->tradeIn_car==1?'Да':'Нет').'<br>'.
                        '№ заявки: '.$request->id.'<br>'.
                        'Дата: '.(new \DateTime($request->created_at))->format('d.m.Y H:i')
                        )
                ->send();
        }
        
        echo json_encode(['success' => true, 'html' => $this->renderAjax('@app/views/site/success.php')]);
        exit;
    }
}

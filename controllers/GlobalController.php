<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Users;

class GlobalController extends Controller
{
    public $enableCsrfValidation = false;

    public function setActivepage($item){
        $this->view->params['active_page'] = $item;
    }
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'foreColor'=>0x66534F,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function init()
    {
        if (!Yii::$app->request->isAjax){
        /*    $this->view->registerJsFile('/resources/jquery-ui-1.11.4/jquery-ui.min.js', ['depends'=>'yii\web\JqueryAsset']);
            $this->view->registerJsFile('/resources/js/jquery.ui.datepicker-ru.js', ['depends'=>'yii\web\JqueryAsset']);
            $this->view->registerCssFile('/resources/jquery-ui-1.11.4/jquery-ui.min.css');
            $this->view->registerCssFile('/resources/jquery-ui-1.11.4/jquery-ui.theme.min.css');*/
            $this->view->registerCssFile('/resources/css/main.css');
            $this->view->registerJsFile('/resources/js/jquery.cookie.js', ['depends'=>'yii\web\JqueryAsset']);
        }
    }
}

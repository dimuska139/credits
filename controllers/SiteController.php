<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\Car;
use app\models\Request;
use app\models\Region;
use app\models\Solution;
use app\models\Brand;
use app\models\Model;
use app\models\User;
use app\models\Complectation;
use yii\helpers\ArrayHelper;


class SiteController extends GlobalController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'foreColor'=>0x66534F,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    private function translit($str) {
        $rus = array('-',' ', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
        $lat = array('_','_','A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
        return str_replace($rus, $lat, $str);
      }
    
    public function actionTranslitbrands()
    {
        $brands = Brand::find()->all();
        
        foreach ($brands as $brand) {
            $brand->alias = mb_strtolower($this->translit($brand->name));
            $brand->save();
        }
    }
    
    public function actionTranslitmodels()
    {
        $models = Model::find()->all();
        
        foreach ($models as $model) {
            $model->alias = mb_strtolower($this->translit($model->name));
            $model->save();
        }
    }
    
    

    public function actionIndex()
    {        
        $cars = ArrayHelper::map(Car::find()->all(), 'id', 'name');
        $regions = ArrayHelper::map(Region::find()->all(), 'id', 'name');
        $solutions = ArrayHelper::map(Solution::find()->all(), 'id', 'name');
        $new = Request::find()
            ->where([
                    'status' => 1
                ])
            ->andWhere([
                '<=', 'created_at', (new \DateTime())->format('Y-m-d H:i:s')
            ])
            ->andWhere([
                'completed' => 0
            ])
            ->limit(6)
            ->orderBy(['id' => SORT_DESC])
            ->all();
        $completed = Request::find()
            ->where([
                    'status' => 1,
           //         'completed' => 1
                ])
            ->andWhere([
                '<=', 'completed_at', (new \DateTime())->format('Y-m-d H:i:s')
            ])
            ->andWhere([
                '<>', 'completed_at', 'NULL'
            ])
            ->limit(6)
            ->orderBy(['id' => SORT_DESC])
            ->all();
        $brands = Brand::find()
                ->orderBy(['name'=> SORT_ASC])
                ->where([
                    'status' => 1
                ])
                ->all();
        
        $regions_list = Region::find()
                ->orderBy(['name'=> SORT_ASC])
                ->where([
                    'status' => 1
                ])
                ->all();
        
        $now = new \DateTime();
        if ($now->format('H')>=21 || $now->format('H')<8)
            $status = "offline";
        else
            $status = "online";
        if (Yii::$app->request->isAjax){
            echo json_encode(['html' => $this->renderAjax('@app/views/site/fakerequest.php', [
                'status' => $status,
                'new' => $new,
                'completed' => $completed,
                'cars' => $cars,
                'regions' => $regions,
                'solutions' => $solutions
            ])]);
            exit;
        }
        
        $models = false;
        $default_brand = false;
        if (!empty($_GET['brand'])){
            
            $brand = Brand::find()->where([
                "alias" => $_GET['brand']
            ])->one();
            if (empty($brand))
                throw new \yii\web\HttpException("404");
            $brand_id = $brand->id; 
            $default_brand = $brand;
            $models = Model::find()
                    ->orderBy(['name'=> SORT_ASC])
                    ->where([
                        'status' => 1,
                        'brand_id' => $brand_id
                    ])
            ->all();
        }
        
        
        $this->view->registerCssFile('/resources/css/main-header.css');
        $this->view->registerCssFile('/resources/css/index.css');
        $this->view->registerCssFile('/resources/css/dropkick.css');
        $this->view->registerJsFile('/resources/js/jquery.dropkick.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/jquery.maskedinput.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/jquery.nouislider.min.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/jquery.checkbox.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/main.js',['depends'=>'yii\web\JqueryAsset']);
        return $this->render('index', [
            'regions_list' => $regions_list,
            'brands' => $brands,
            'status' => $status,
            'new' => $new,
            'completed' => $completed,
            'cars' => $cars,
            'regions' => $regions,
            'solutions' => $solutions,
            'models' => $models,
            'default_brand' => $default_brand
        ]);
    }
    
    public function actionModel(){
        $cars = ArrayHelper::map(Car::find()->all(), 'id', 'name');
        $regions = ArrayHelper::map(Region::find()->all(), 'id', 'name');
        $solutions = ArrayHelper::map(Solution::find()->all(), 'id', 'name');
        $new = Request::find()
            ->where([
                    'status' => 1
                ])
            ->andWhere([
                '<=', 'created_at', (new \DateTime())->format('Y-m-d H:i:s')
            ])
            ->andWhere([
                'completed' => 0
            ])
            ->limit(6)
            ->orderBy(['id' => SORT_DESC])
            ->all();
        $completed = Request::find()
            ->where([
                    'status' => 1,
           //         'completed' => 1
                ])
            ->andWhere([
                '<=', 'completed_at', (new \DateTime())->format('Y-m-d H:i:s')
            ])
            ->andWhere([
                '<>', 'completed_at', 'NULL'
            ])
            ->limit(6)
            ->orderBy(['id' => SORT_DESC])
            ->all();
        $brands = Brand::find()
                ->orderBy(['name'=> SORT_ASC])
                ->where([
                    'status' => 1
                ])
                ->all();
        
        $regions_list = Region::find()
                ->orderBy(['name'=> SORT_ASC])
                ->where([
                    'status' => 1
                ])
                ->all();
        
        $now = new \DateTime();
        if ($now->format('H')>=21 || $now->format('H')<8)
            $status = "offline";
        else
            $status = "online";
        if (Yii::$app->request->isAjax){
            echo json_encode(['html' => $this->renderAjax('@app/views/site/fakerequest.php', [
                'status' => $status,
                'new' => $new,
                'completed' => $completed,
                'cars' => $cars,
                'regions' => $regions,
                'solutions' => $solutions
            ])]);
            exit;
        }
        $model = Model::find()->where([
            "alias" => $_GET['model'],
            "status" => 1
        ])->one();
        
        if (empty($model))
            throw new \yii\web\HttpException("404");
        
        $model_id = $model->id; 
        $default_model = Model::findOne($model_id);
        $default_brand = Brand::findOne($default_model->brand_id);
        $complectations = Complectation::find()
            ->orderBy(['price'=> SORT_ASC])
            ->where([
                'status' => 1,
                'model_id' => $model_id
            ])
            ->all();
        
        
        $this->view->registerCssFile('/resources/css/main-header.css');
        $this->view->registerCssFile('/resources/css/index.css');
        $this->view->registerCssFile('/resources/css/dropkick.css');
        $this->view->registerJsFile('/resources/js/jquery.dropkick.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/jquery.maskedinput.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/jquery.nouislider.min.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/jquery.checkbox.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/main.js',['depends'=>'yii\web\JqueryAsset']);
        return $this->render('model', [
            'regions_list' => $regions_list,
            'brands' => $brands,
            'status' => $status,
            'new' => $new,
            'completed' => $completed,
            'cars' => $cars,
            'regions' => $regions,
            'solutions' => $solutions,
            'complectations' => $complectations,
            'default_model' => $default_model,
            'default_brand' => $default_brand
        ]);
    }
}

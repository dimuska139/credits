<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Brand;
use app\models\Model;
use app\models\Complectation;
use app\models\Request;
use app\models\Region;
use app\models\User;
use app\models\Grade;

use yii\helpers\ArrayHelper;

class AdminController extends Controller
{    
    public $enableCsrfValidation = false;
    // Генерация хэша от переданной строки. Нужно для удобства при создании пользователей через phpmyadmin (например)
    public function actionGethash()
    {
        if (!empty($_GET['password'])){
            echo password_hash($_GET['password'], PASSWORD_BCRYPT);
            
        } else {
            echo "Password required";
        }
        die();
    }
    
    public function actionLogout()
    {
        Yii::$app->session->remove('id');
        Yii::$app->session->remove('admin');
        header( 'Location: /', true, 302 );
        exit();
    }
    
    public function actionIndex(){
        $type = Yii::$app->request->get('type', 'new');
        $brands = ArrayHelper::map(Brand::find()->all(), 'id', 'name');
        $models = ArrayHelper::map(Model::find()->all(), 'id', 'name');
        $regions = ArrayHelper::map(Region::find()->all(), 'id', 'name');
        $grades = ArrayHelper::map(Grade::find()->orderBy(['value'=> SORT_ASC])->all(), 'id', 'name');
        $complectations = ArrayHelper::map(Complectation::find()->all(), 'id', 'name');
        $this->layout = "admin.php";
        $this->view->registerCssFile('/resources/css/admin.css');
        if (empty(Yii::$app->session->get('admin'))){
            if (Yii::$app->request->isPost){
                $error_text = "";
                $login = Yii::$app->request->post('login', '');
                $password = Yii::$app->request->post('password', '');
                if (!empty($login) || !empty($password)){
                    $user = User::find()
                        ->where([
                            'login' => $_POST['login']
                        ])->one();
                if (!empty($user)){
                    if (password_verify($password, $user->hash)){
                        Yii::$app->session->set('id', $user->id);
                        Yii::$app->session->set('admin', true);
                        header('Location: /admin/?type=new', true, 301);
                        exit();
                    } else
                        $error_text = "Логин или пароль неверный";
                } else
                    $error_text = "Логин или пароль неверный";
                }
                return $this->render('auth', [
                    "error_message" => $error_text
                ]);
            }
            return $this->render('auth');
        }
        $user = User::findOne(Yii::$app->session->get('id'));
        $requests_list = Request::find()
            ->where([
                "manager_id" => Yii::$app->session->get('id'),
                "fake" => 0
            ]);
        
        $requests_all_amount = clone $requests_list;
        $requests_all_amount = $requests_all_amount->count();
        
        if ($type=="new")
            $requests_list->andWhere ("completed=0");
        $requests_new_amount = clone $requests_list;
        $requests_new_amount->andWhere ("completed=0");
        $requests_new_amount = $requests_new_amount->count();
        
        $requests_list = $requests_list->orderBy(['created_at'=> SORT_ASC])->all();
        $this->view->params['user'] = $user;
        $this->view->registerJsFile('/resources/js/admin.js', ['depends'=>'yii\web\JqueryAsset']);
        return $this->render('index', [
            "grades" => $grades,
            "type" => $type,
            "brands" => $brands,
            "models" => $models,
            "complectations" => $complectations,
            "regions" => $regions,
            "requests" => $requests_list,
            "requests_new_amount" => $requests_new_amount,
            "requests_all_amount" => $requests_all_amount,
        ]);
    }
}

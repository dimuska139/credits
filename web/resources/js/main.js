$(document).ready(function(){
    function splitNums(delimiter, str)
    {  
        str = str.replace(/(\d+)(\.\d+)?/g,
            function(c,b,a){return b.replace(/(\d)(?=(\d{3})+$)/g, '$1'+delimiter) + (a ? a : '')} );
        return str;
    }

    
    function updateGrid(){
        $.ajax({
            type: "get",
            url: "/",
            dataType: "json",
            beforeSend: function(){
            },
            complete: function(){
            },
            success: function(response){
                $('.requests').html(response.html);
            }
        });
    }
    
    
    
    $('.brands .brand-logo').on('click', function(){
        var brand_id = $(this).data('id');
        $(".request select[name=brand] option[value=" + brand_id + "]").attr('selected', 'true');
        $(".request select[name=brand]").change();
    });
    
    $(".request select[name=brand]").change(function () {
        if ($(this).val()!=0){
            var url = "/ajax/models?brand_id="+$(this).val();
            $.get(url, function(request) {
                $(".request select[name=model]").html(request);
                $(".request select[name=model]").dropkick("refresh").change();
                
                if (typeof default_model_id != 'undefined' && default_model_id != false){
                    $(".request select[name=model]").val(default_model_id);
                    $(".request select[name=model]").change();
                    default_model_id = false;
                }
                
                var img_path = $(".request select[name=model] :selected").data('img');
                if (img_path.length>0)
                    $('.model-preview').css('background-image', 'url('+img_path+')');
                else
                    $('.model-preview').css('background-image', 'none');
             //   <div class="city-img"></div>
        //        $(".request select[name=complectation]").html('<option value="0">Не выбрана</option>');
        //        $(".request select[name=complectation]").dropkick("refresh").change();
            });
        } else {
            $(".request select[name=model]").html('<option value="0">Не выбрана</option>');
            $(".request select[name=model]").dropkick("refresh").change();
            $(".request select[name=complectation]").html('<option value="0">Не выбрана</option>');
            $(".request select[name=complectation]").dropkick("refresh").change();
            $("#first-payment").html("0 руб.");
            $(".descr .result").html('');
            $("#price").val('');
        }
    });
    
    $(".request select[name=model]").change(function () {
        if ($(this).val()!=0){
            var url = "/ajax/complectations?model_id="+$(this).val();
            $.get(url, function(request) {
                $(".request select[name=complectation]").html(request);
                $(".request select[name=complectation]").dropkick("refresh").change();
                var price = $("#complectation :selected").data('price');
                price = splitNums(' ', String(price))+" руб.";
                $("#price").val(price);
            });
        } else {
            $(".request select[name=complectation]").html('<option value="0">Не выбрана</option>');
            $(".request select[name=complectation]").dropkick("refresh").change();
            $("#price").val('');
        }
        
        var img_path = $(".request select[name=model] :selected").data('img');
        if (typeof img_path != 'undefined' && img_path.length>0)
            $('.model-preview').css('background-image', 'url('+img_path+')');
        else
            $('.model-preview').css('background-image', 'none');
    });
    
    $(".request select[name=complectation]").change(function(){
        if ($(this).val().length>0){
            var price = $("#complectation :selected").data('price');
            price = splitNums(' ', String(price))+" руб.";
            $("#price").val(price);
        //    $("#slider-firstpay").val(20);
            calcFirst();
            calcPayment();
        } /*else {
            $("#slider-firstpay").val(0);
        }*/
    });
    
    $('#price').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9(\s{1})]/g)) {
            this.value = this.value.replace(/[^0-9(\s{1})]/g, '');
        }
        var str = this.value.replace(/\s+/g, '');
        if (this.value.charAt(0)=='0')
            this.value = this.value.substr(1);
        if (this.value.length>9)
            this.value = this.value.substr(0, 9);
        this.value = str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
    });
    
    $('#age').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9(\s{1})]/g)) {
            this.value = this.value.replace(/[^0-9(\s{1})]/g, '');
        }
        var str = this.value.replace(/\s+/g, '');
        if (this.value.charAt(0)=='0')
            this.value = this.value.substr(1);
        if (this.value.length>2)
            this.value = this.value.substr(0, 2);
        this.value = str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
    });
    
    function number_format(a, b) {
        function c(a, b) {
            var c = {};
            for (key in a)c[key] = "undefined" != typeof b[key] ? b[key] : a[key];
            return c
        }

        function d(a, b) {
            if (a.length <= 3)return a;
            for (var c = a.length, d = "", e = 0, f = c - 1; f >= 0; f--) {
                var g = a.substr(f, 1);
                0 != e % 3 || 0 == e || isNaN(parseFloat(g)) || (d = b + d), d = g + d, e++
            }
            return d
        }

        if ("number" != typeof a && (a = parseFloat(a), isNaN(a)))return!1;
        var e = {before: "", after: "", decimals: 2, dec_point: ".", thousands_sep: ","};
        if (b = b && "object" == typeof b ? c(e, b) : e, a = a.toFixed(b.decimals), -1 != a.indexOf("."))var f = a.split("."), a = d(f[0], b.thousands_sep) + b.dec_point + f[1]; else var a = d(a, b.thousands_sep);
        return b.before + a + b.after
    }
    
    function calc(a, b, c, d) {
        if (a > b)return 0;
        var e = -1 * (b - a), f = d / 1200, g = c, h = f * e * Math.pow(1 + f, g) / (1 - Math.pow(1 + f, g));
        return h
    }

    function calcPayment() {
        if (typeof $("#complectation :selected").data('price') == 'undefined')
            return;
        
        if ($("input[name=first-payment-value]").length == 0) {
            return;
        }
        if ($("input[name=price]").length == 0) {
            return;
        }
        if ($("#slider-payment-year").length == 0) {
            return;
        }
        
        var params = {
            complectation_id: $("#complectation :selected").val(),
            first_payment: $("input[name=first-payment-value]").val(),
            years: Math.round($("#slider-payment-year").val())
        }
        
        $.ajax({
            type: "post",
            url: "/ajax/calculate",
            dataType: "json",
            data: params,
            beforeSend: function(){
            },
            complete: function(){
            },
            success: function(response){
                var value = number_format(parseInt(response.result), {thousands_sep: " ", decimals: 0});
                value += " руб.";
                $(".result span").text(value);
            }
        });
    }
    
    function calcFirst() {
        var price = $("#complectation :selected").data('price');
        if (typeof price != 'undefined'){
            var slider_val = $("#slider-firstpay").val();
            var first_payment = Math.round(price * slider_val / 100);
            $("input[name=first-payment-value]").val(first_payment);
            $(".firstpay-progressbar-value").html(Math.round(slider_val)+'%');
            first_payment = String(first_payment);
            if (isNaN(first_payment)){
                first_payment = "0 руб."
            } else {
                first_payment = first_payment.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1 ") + " руб."; 
            }
            $("#first-payment").html(first_payment);
        }
        
        var a = price;
      //  a = parseInt(a.replace(new RegExp("[^0-9]", "g"), ""));
        var b = $("#slider-firstpay").val(), c = Math.round(a * b / 100);
        c = String(c), c = isNaN(c) ? "0 руб." : c.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1 ") + " руб.", $("input[name=add_firstPay]").val(c)
    }
    
    $("#calc-slide").on("click", function () {
        return $("#credit-calc-container").slideToggle();
    });
    
    
    
    var timer = setInterval(updateGrid, 1000);
    
    $("select").dropkick();
    $("input[name=phone]").mask("9 (999) 999-99-99");
    $("#slider-firstpay").noUiSlider({range: [0, 70], start: 20, step: 1, handles: 1, slide: function () {
            $(".firstpay-progressbar-value").html(Math.round($(this).val())+'%');
            calcFirst(), calcPayment()
        }
    });
    
    $('input[name=fio]').bind('change keyup input', function(event){
        if (this.value.match(/[^A-Za-zА-Яа-я(\s{1})]/g)) {
            this.value = this.value.replace(/[^A-Za-zА-Яа-я(\s{1})]/g, '');
        }
        if (this.value.charAt(0)==' ')
            this.value = this.value.substr(1);
        this.value = this.value.charAt(0).toUpperCase() + this.value.substr(1);
    });
    
    

    $("#slider-payment-year").noUiSlider({range: [6, 84], start: 60, step: 1, handles: 1, slide: function () {
            calcPayment();
            var a = parseFloat($(this).val());
            a = a.toFixed(1);
            if (String(a) == '0.5')
                $("#payment_year").val("6 мес."), $(".calc-info b").text("6 мес.")
            else {
                a = String(a);
                if (a.charAt(a.length-1)=='0')
                    a = a.slice(0, -2);
                $("#payment_year").val(a + " мес."), $(".calc-info b").text(a + " мес.");
            }
        }
    });
    
    $('.js-label').click(function(){
        changeCheck($(this).siblings('.niceCheck'));
    });
    $(".niceCheck").mousedown(function () {
        changeCheck($(this))
    });
    $(".niceCheck").each(function () {
        changeCheckStart($(this))
    });
    
    
    $('.dream-button').click( function(){ // если в href начинается с # или ., то ловим клик
        var scroll_el = $(this).attr('href'); // возьмем содержимое атрибута href
        if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
	    $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 500); // анимируем скроолинг к элементу scroll_el
        }
        return false; // выключаем стандартное действие
    });
    
    $('.request .button').on('click', function(){
        var error = false;
        $('.request select').each(function(){
            if ($(this).val()==0){
                $(this).parent('div').addClass('error');
                console.dir($(this));
                error = true;
            } else {
                $(this).parent('div').removeClass('error');
            }
        });
        
        
        $('.request input[type="text"]').each(function(){
            if ($(this).val().length==0){
                $(this).addClass('error');
                error = true;
            } else {
                $(this).removeClass('error');
            }
        });
        
        if ($('input[name=age]').val()<23 || $('input[name=age]').val()>64){
            $('input[name=age]').addClass('error');
            error = true;  
        }
        
        if ($('input[name=fio]').val().length<3){
            $('input[name=fio]').addClass('error');
            error = true;  
        }
        
        if (error)
            return;
        var params = {
            complectation: $(".request select[name=complectation]").val(),
            price: $("#complectation :selected").data('price'),
            fio: $(".request input[name=fio]").val(),
            phone: $(".request input[name=phone]").val(),
            age: $(".request input[name=age]").val(),
            region: $(".request select[name=region]").val(),
            first_payment: $('.request input[name=first-payment-value]').val(),
            first_payment_percent: $("#slider-firstpay").val(),
            years: $("#slider-payment-year").val(),
            tradein: $("#tradein").prop("checked")?1:0
        };
        $.ajax({
            type: "post",
            data: params,
            url: "/ajax/addrequest",
            dataType: "json",
            beforeSend: function(){
            },
            complete: function(){
            },
            success: function(response){
                if (response.success){
                    $('.request .submitting').remove();
                    $('.request .body').html(response.html);
                    $('.request .body').css('height', '521px');
                    $('.request .title').html("Спасибо за заявку");
                    
                } else
                    alert("Непредвиденная ошибка. Мы уже работаем над её исправлением");
            }
        });
    });
    
    
    $('.complectations-line .item').on('click', function(){
        if (!$(this).find('.description').is(':visible') && $(this).find('.description').html().length>0){
            $('.complectations-line .item .description').slideUp();
            $(this).find('.description').slideDown();
        } /*else
            $(this).find('.description').slideDown();*/
    });
    
    
    if (typeof default_brand_id != 'undefined' && default_brand_id != false){
        $(".request select[name=brand]").val(default_brand_id);
        $(".request select[name=brand]").dropkick("refresh").change();
        default_brand_id = false;
    }
    
    $('.count-credit .button').on('click', function(){
        $(".request select[name=complectation]").val($(this).data('id'));
        $(".request select[name=complectation]").change();
        $('html, body').animate({ scrollTop: $('.request').offset().top }, 500);
    });
});
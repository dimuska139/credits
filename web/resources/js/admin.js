$(document).ready(function(){
    $('.save').on('click', function(){
        var params = {
            request_id: $(this).data('id'),
            grade_id: $(this).parent('div').find('select').val()
        }
        if (params.grade==0){
            alert("Сначала выберите оценку");
            return;
        }
        $.ajax({
            type: "post",
            data: params,
            url: "/ajax/completerequest",
            dataType: "json",
            beforeSend: function(){
            },
            complete: function(){
            },
            success: function(response){
                if (response.success){
                    var new_amount = parseInt($('#new_requests_amount').html())-1;
                    $('#new_requests_amount').html(new_amount);
                    if (type=="new"){
                        $('.item-'+params.request_id).remove();
                        
                    } else {
                        $('.item-'+params.request_id).removeClass('new');
                        $('.item-'+params.request_id).find('.col-4').html("Оценка: "+grades[params.grade_id]);
                    }
                } else
                    alert("Непредвиденная ошибка. Мы уже работаем над её исправлением");
            }
        });
    });
});
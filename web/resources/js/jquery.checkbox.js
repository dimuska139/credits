function changeCheck(el)
{
    var el = el,
        input = el.find("input").eq(0);
    if(!input.attr("checked")) {
        el.css("background-position","-16px 0");
        input.attr("checked", true)
    } else {
        el.css("background-position","left top");
        input.attr("checked", false)
    }
    return true;
}

function changeCheckStart(el)
{
    var el = el,
        input = el.find("input").eq(0);
    if(input.attr("checked")) {
        el.css("background-position","-16px 0");
    }
    return true;
}
<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $login
 * @property string $name
 * @property string $hash
 * @property string $last_online
 * @property string $created_at
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'hash', 'last_online'], 'required'],
            [['last_online', 'created_at'], 'safe'],
            [['login'], 'string', 'max' => 100],
            [['name'], 'string', 'max' => 200],
            [['hash'], 'string', 'max' => 60]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'name' => 'Name',
            'hash' => 'Hash',
            'last_online' => 'Last Online',
            'created_at' => 'Created At',
        ];
    }
}

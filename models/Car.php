<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "car".
 *
 * @property integer $id
 * @property string $name
 * @property integer $price_from
 * @property integer $price_to
 * @property integer $weight
 * @property string $created_at
 * @property integer $status
 */
class Car extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price_from', 'price_to', 'status'], 'required'],
            [['price_from', 'price_to', 'weight', 'status'], 'integer'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price_from' => 'Price From',
            'price_to' => 'Price To',
            'weight' => 'Weight',
            'created_at' => 'Created At',
            'status' => 'Status',
        ];
    }
}

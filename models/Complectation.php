<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "complectation".
 *
 * @property integer $id
 * @property string $name
 * @property integer $price
 * @property integer $model_id
 * @property string $created_at
 * @property integer $status
 */
class Complectation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'complectation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price', 'model_id'], 'required'],
            [['price', 'model_id', 'status'], 'integer'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
            'model_id' => 'Model ID',
            'created_at' => 'Created At',
            'status' => 'Status',
        ];
    }
}

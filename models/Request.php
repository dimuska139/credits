<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fake_request".
 *
 * @property integer $id
 * @property string $number
 * @property integer $car_id
 * @property integer $region_id
 * @property integer $solution_id
 * @property integer $price
 * @property string $created_at
 * @property integer $status
 */
class Request extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'region_id', 'price', 'status'], 'required'],
            [['car_id', 'region_id', 'solution_id', 'price', 'status'], 'integer'],
            [['created_at'], 'safe'],
            [['number'], 'string', 'max' => 6]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Number',
            'car_id' => 'Car ID',
            'region_id' => 'Region ID',
            'solution_id' => 'Reason ID',
            'price' => 'Price',
            'created_at' => 'Created At',
            'status' => 'Status',
        ];
    }
    
    public static function calculatePayment($price, $first_payment, $years){
        $months_amount = $years; // Количество месяцев
        $credit_sum = $price - $first_payment; // сумма кредита = сумма авто - первый взнос
        // процентная ставка на весь период = кол. месяцев * проц. ставка за месяц
        $interest_rate = $months_amount * Yii::$app->params['interestRate'];
        // полная сумма кредита = сумма кредита + проц. ставка на весь период
        $full_credit_sum = $credit_sum + $credit_sum*($interest_rate/100);
        // Ежемесячный платёж
        $everymonth_payment = round($full_credit_sum/$months_amount);
        return $everymonth_payment;
    }
}

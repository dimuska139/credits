<?php

return [
    'notificationEmail' => 'notification@epolus.ru',
    'adminEmails' => [
        'autostaryug@gmail.com',
        'dimuska139@mail.ru',
        '5559560@mail.ru'
    ],
    'interestRate' => 0.25 // Месячная процентная ставка
];

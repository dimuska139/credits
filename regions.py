#!/usr/bin/env python
from mysql.connector import MySQLConnection, Error
from mysql.connector import Error


f = open("regions.txt","r")
regions = f.readlines()


if __name__ == '__main__':
    conn = MySQLConnection(host='localhost',
                                       database='epolyus',
                                       user='root',
                                       password='12345')
    cursor = conn.cursor()
    sql = "INSERT INTO region(name, status) " \
          "VALUES(%s,1)"
    for m in regions:
        args = (m,)
	cursor.execute(sql, args)
        if cursor.lastrowid:
            print('last insert id', cursor.lastrowid)
        else:
            print('last insert id not found')
        conn.commit()
    cursor.close()
    conn.close()


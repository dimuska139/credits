<?php

namespace app\commands;

use yii\console\Controller;
use Yii;

use app\models\Car;
use app\models\Request;
use app\models\Region;
use app\models\Solution;
use yii\helpers\ArrayHelper;

// php /путь/до/папки/с/проектом yii controller/action
class CronController extends Controller {
    public function actionGeneraterequest() {
        $requestTime = new \DateTime("now", new \DateTimeZone('Europe/Moscow'));
        $endTime = new \DateTime("now", new \DateTimeZone('Europe/Moscow'));
        $endTime->add(new \DateInterval('PT1M')); // Время окончания генерации: текущее время + 1 час
        
        $alf = [];
        foreach(range('A','Z') as $i)
            $alf[] = $i;
        $carsWeights = ArrayHelper::map(Car::find()->all(), 'id', 'weight');
        $maxCarWeight = Car::find()->max('weight');
        $regionsWeights = ArrayHelper::map(Region::find()->all(), 'id', 'weight');
        $maxRegionWeight = Region::find()->max('weight');
        $solutionsWeights = ArrayHelper::map(Solution::find()->all(), 'id', 'weight');
        $maxSolutionWeight = Solution::find()->max('weight');
        while ($requestTime<=$endTime && ($requestTime->format('H')<21) && ($requestTime->format('H')>=8)){
            $carsIds = [];
            $rand = rand(0, $maxCarWeight); // Случайное число в диапазоне [0, макс. вес]
            foreach ($carsWeights as $id => $weight){
                if ($rand<=$weight)
                    $carsIds[] = $id;
            }
            $carId = $carsIds[array_rand($carsIds)];
            
            $regionsIds = [];
            $rand = rand(0, $maxRegionWeight); // Случайное число в диапазоне [0, макс. вес]
            foreach ($regionsWeights as $id => $weight){
                if ($rand<=$weight)
                    $regionsIds[] = $id;
            }
            $regionId = $regionsIds[array_rand($regionsIds)];
            
            $solutionsIds = [];
            $rand = rand(0, $maxSolutionWeight); // Случайное число в диапазоне [0, макс. вес]
            foreach ($solutionsWeights as $id => $weight){
                if ($rand<=$weight)
                    $solutionsIds[] = $id;
            }
            $solutionId = $solutionsIds[array_rand($solutionsIds)];


            $car = Car::findOne($carId);
            $request = new Request();

            $request->number = $alf[array_rand($alf)].(string)rand(0,9).(string)rand(0,9).(string)rand(0,9).(string)rand(0,9);
            $request->car_id = $carId;
            $request->region_id = $regionId;
            $request->solution_id = $solutionId;
            $request->completed = 0;
            $request->price = rand($car->price_from, $car->price_to);
            $request->fake = 1;
            $request->created_at = $requestTime->format('Y-m-d H:i:s');
            $dt = clone $requestTime;
            $dt->add(new \DateInterval('PT20M'));
            // Время, когда заявка ДОЛЖНА БЫТЬ обработана
            
        /*    $last_to_complete = Request::find()
                    ->where([
                        'status' => 1
                    ])
                    ->orderBy(['completed_at' => SORT_DESC])
                    ->one();*/
            
            $request->completed_at = $dt->format('Y-m-d H:i:s');
            $request->status = 1;
            $request->save();
            
            $randSec = rand(3,10);
            $requestTime->add(new \DateInterval('PT'.$randSec.'S'));
        }
    }
    
    public function actionCompleterequest(){
    /*    if ((new \DateTime("now", new \DateTimeZone('Europe/Moscow')))->format('H')<21 && (new \DateTime("now", new \DateTimeZone('Europe/Moscow')))->format('H')>=8){
            $requests = Request::find()
                    ->where("status=1")
                    ->andWhere("completed=0")
                    ->andWhere("fake=1")
                    ->andWhere([
                        '<', 'completed_at', (new \DateTime())->format('Y-m-d H:i:s')
                    ])
                    ->all();
            foreach ($requests as $key => $request) {
                $request->completed = 1;
                $request->save();
            }
        }*/
    }
    
    public function actionDeleteoldrequest(){
        $currentTime = new \DateTime("now", new \DateTimeZone('Europe/Moscow'));
        $currentTime->sub(new \DateInterval('P1D')); // Предыдущий день
        
        $params = [':c_at' => $currentTime->format('Y-m-d H:i:s')];
        Yii::$app->db->createCommand('DELETE FROM `request` WHERE status=1 AND completed=1 AND fake=1 AND completed_at<:c_at')
           ->bindValues($params)
           ->execute();
    }
}
?>

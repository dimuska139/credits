<div class="<?=($num%2==0?'even ':'odd ');?>line<?=(!empty($item['price'] && $item['difference_price']>=50 && $item['condition']!=1)?' difference_price':'');?><?=(!empty($item['minutes_to_publish'])?' moderation':'');?>">
                <div class="time">
                    <div class="pos">
                        <?=$item['time'];?>
                    </div>
                </div>
                <div class="td logo">
                    <?=$item['marka_logo']?('<img class="brand-logo" src="/resources/img/brands/'.$item['marka_logo'].'">'):'';?>
                </div>
                <div class="td car"><div class="fade"></div>
                    <div class="pos">
                        <nobr>
    <?php             
         $eng = $item['enginevol'].((mb_strlen($item['engine'],'UTF-8')!=0 && $item['engine']!='b')?$item['engine']:'').(!empty($item['transmission'])?(' '.$item['transmission']):"");
         if (mb_strlen($eng,'UTF-8')>2)
             $eng = '('.$eng.')';
         if (strcmp(mb_convert_encoding($item['marka'], 'utf-8'), mb_convert_encoding('Лада', 'utf-8'))==0)
                $item['marka'] = 'ВАЗ (Lada)';
         $mark = $item['marka'].' '.$item['model'].' '.$item['model_2'];
         if (mb_strlen($mark,'UTF-8')>20)
             $mark = mb_substr($mark, 0, 20,'UTF-8').'...';
     ?>
     <span class="mark-info"><?=$mark;?> <span class="engine-info"><?=$eng;?></span></span>
                        </nobr>
                    </div>

    <?php if ($item['rudder']==1): ?>
        <img class="rudder-item" src="/resources/img/rudder.png">
    <?php endif; ?>
    <?php if ($item['vin']==1): ?>
        <img class="vin-item" src="/resources/img/vin.png">
    <?php endif; ?>
    <?php if ($item['fast']==1): ?>
        <img class="fast-item" src="/resources/img/fast.png">
    <?php endif; ?>
    <?php if ($item['wheel']==1): ?>
        <img class="wheel-item" src="/resources/img/wheel.png">
    <?php endif; ?>
    </div>

    <div class="td price">
        <div class="pos">
            <?php if (!empty($item['price']) && $item['torg']==1): ?>
                <img class="torg-item" src="/resources/img/torg.png"><?php endif; ?><?php if (!empty($item['price'])): ?><?=number_format($item['price'],0,' ',' ');?>
            <?php endif; ?>
        </div>
    </div>
    <?php if ($item['condition']==0): ?>
        <?php if ($item['average_price']==0): ?>
            <div class="diff td"><div class="pos"></div></div>
        <?php else: ?>
            <?php if ($item['difference_price']>0): ?>
                <div class="diff td"><div class="pos green"><?=number_format($item['difference_price'],0,' ',' ');?></div></div>
            <?php else: ?>
                <div class="diff td"><div class="pos red"><?=number_format((-1)*$item['difference_price'],0,' ',' ');?></div></div>
            <?php endif; ?>
        <?php endif; ?>
    <?php else: ?>
        <div class="diff badcondition td"><img src="/resources/img/bt.png"></div>
    <?php endif; ?>
        
        <div class="td year">
            <div class="pos">
                <?=$item['year'];?>
            </div>
        </div>
        
        <div class="td body" data-tooltip="<?=$item['body'];?>"><div class="tooltip"></div>
            <div class="pos">
                <?php if (!empty($item['body_img'])): ?>
                    <?=$item['body_img']; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="region td">
            <?php
                $metr_reg = $item['region'].(mb_strlen($item['metro'], 'UTF-8')?(", м. ".$item['metro']):"");
                if (mb_strlen($metr_reg, 'UTF-8')>10)
                    $metr_reg = mb_substr($metr_reg, 0, 10, 'UTF-8').'...';
            ?>
            <div class="pos"><?=$metr_reg;?></div>
        </div>
        <?php
            $phone_find_color = '#f5f5f5';
            if ($item['minutes_to_publish']>0)
                $phone_find_color = '#ffeb00';
            if ($item['difference_price']>=50 && !empty($item['price']) && $item['condition'] != 1)
                $phone_find_color = '#a5f028';
        ?>
        <div style="background-color: <?=$phone_find_color;?>" class="phonefind td">
            <div class="pos">
                <?php if ($item['phone_find']<20):?>
                    <div class="phonefindsubmit" <?=$item['phone_find']>0?'style="color:#28578b;" ':'style="color:#969696" ';?>><?=$item['phone_find'];?></div>
                <?php else: ?>    
                    <div class="phonefindsubmitimg"></div>
                <?php endif; ?>
            </div>
        </div>
                
</div>
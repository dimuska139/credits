<?php $this->title = "Настройки"; ?>
<div class="settings">
    <div class="profile">
        <div class="title">Профиль</div>
        <div class="form-line">
            <label>Имя</label>
            <input disabled="disabled" type="text" id="name" value="<?=$user->name;?>">
            <div class="error" id="name-error"></div>
        </div>
        <div class="form-line">
            <label>Телефон</label>
            <input disabled="disabled" type="text" id="phone" value="<?=$user->login;?>">
            <div class="error" id="phone-error"></div>
        </div>
    </div>
    <div class="site-settings">
        <div class="title">Настройки сайта</div>
        <div class="form-line">
            <label>Звуки</label>
            <input <?php if ($user->search_sounds==1):?>checked<?php endif;?> type="checkbox" id="search_sounds">
        </div>
    <!--    <div class="form-line">
            <label>Фотографии в поиске</label>
            <input <?php if ($user->search_photos==1):?>checked<?php endif;?> type="checkbox" id="search_photos">
        </div> -->
    </div>
    <div class="change-password">
    <div class="title">Изменить пароль</div>
    <div class="form-line">
        <label>Новый пароль</label>
        <input type="password" id="password">
        <div class="error" id="password-error"></div>
    </div>
    <div class="form-line">
        <label>Повторите пароль</label>
        <input type="password" id="repeat-password">
    </div>
    <div class="save">Сохранить</div>
    <div class="saved">Изменения сохранены!</div>
</div>
<?php $this->title = "AM97.RU: Помощник по купле-продаже автомобилей"; ?>
<div class="main-info">
    <div class="row">
        <div class="left">
            <div class="block">
                <div class="title">
                    Быстрая поисковая система автомобилей
                </div>
                <p>
                    Получайте все самые свежие автомобильные объявления с ведущих автопорталов
                    по всей России в реальном времени.
                    <ul>
                        <li>Auto.ru, Avito.ru, Am.ru, Irr.ru и Drom.ru в одной удобной ленте</li>
                        <li>Объявления с модерации на Avito.ru (с телефонами)</li>
                        <li>Проверка телефона на наличие в базе объявлений</li>
                        <li>Изменение цены объявлений</li>
                        <li>Отклонение от средней рыночной цены</li>
                        <li>Фильтрация от спекулянтов</li>
						<li>Поиск тс за месяц</li>
                    </ul>
					Не нужно платить за несколько тысяч переходов сразу, платите столько — сколько считаете нужным!
					Всего <b>2 рубля</b> за переход!
                </p>
            </div>
            <div class="block">
                <div class="title">
                    Система размещения объявлений
                </div>
                <p>
                    С помощью нашей упрощенной системы добавления объявлений, Вы легко и просто сможете разом
                    разместить объявления на Auto.ru и Avito.ru, с возможностью дальнейшего редактирования.
                    <ul>
                        <li>Один личный кабинет для всех объявлений</li>
                        <li>Удобная панель управления/редактирования объявлений</li>
                        <li>Объявления размещаются от частного лица</li>
                        <li>Возможность обновления объявлений (тестирование)</li>
                        <li>Малая вероятность блокировки объявления</li>
                    </ul>
					Наша система облегчит работу с объявлениями и сэкономит Ваше время. Стоимость размещения объявления составляет <b>699 рублей</b>!
                </p>
            </div>
            <div class="register block ">
                <div class="title">Бесплатная регистрация</div>
                <div class="register-description">Получите <?=Yii::$app->params['START_BALANCE'];?> рублей на баланс бесплатно!</div>
                <div class="input-block" style="margin-top: 5px">
                    <input class="username" type="text" placeholder="Ваше имя">
                </div>
                <div class="input-block">
                    <input class="userphone" type="text" placeholder="Телефон">
                </div>
                <div id="register-error"></div>
                <div class="button bt_1" id="register-button">
                    <p>Зарегистрироваться</p>
                </div>
                <div id="register-success">
                    Благодарим за регистрацию!</br>Логин и пароль для авторизации отправлены Вам в СМС!
                </div>
            </div>
            <div class="row">
                <div class="block">
                    <div class="title">
                        Контакты
                    </div>
					<p>
                        Телефон: <b>+7 925 5559560</b> (с 9:00 до 21:00)
                    </p>
					<p>
                        ВКонтакте: Фёдоров Евгений
                    </p>
                    <p>
                        E-mail: starevg@mail.ru
                    </p>
                </div>
            </div>
        </div>
        <div class="right">
            <div class="block">
                <div class="last-text">Последние объявления</div>
                <div class="online-table">
                    <div class="online-header">
                        <div class="time">время</div>
                        <div class="logo"></div>
                        <div class="car">автомобиль</div>
                        <div class="price">цена, руб.</div>
                        <div class="diff">
                            <img src="/resources/img/otkl.png" class="otkl-icon">
                        </div>
                        <div class="year">год</div>
                        <div class="body">кузов</div>
                        <div class="region">регион</div>
                        <div class="phonefind">
                            <img src="/resources/img/phonefind.png" class="phonefind-icon">
                        </div>
                    </div>
                    <div class="online-body">
                        <?php foreach($lastonline as $num => $item): ?>
                            <?php if ($num==0): ?>
                                <script>
                                    var last_item_id = <?=$item['Id'];?>;
                                </script>
                                <?php break; ?>
                            <?php endif; ?>
                        <?php endforeach;?>
                        <?php echo \Yii::$app->view->renderFile('@app/views/users/ads_list_body.php',['lastonline' => $lastonline]); ?>
                    </div>
                </div>
            </div>

            <?php if (!empty($last_list)): ?>
            <div class="last block">
                <div class="last-text">Последние добавления</div>
                <?php foreach($last_list as $item): ?>
                <div class="item" data-id="<?=$item->id;?>">
                    <?php   
                        $photo = $item->photo;
                        $img = "/resources/img/nophoto.png";
                        if ($photo && file_exists($photo->url))
                            $img = '/'.$photo->url;
                        if ($photo && !empty($photo->thumb) && file_exists($photo->thumb))
                            $img = '/'.$photo->thumb;
                    ?>
                    <div class="photo" style="background: url('<?=$img; ?>') no-repeat center; background-size: cover;">

                    </div>
                    <div class="text">
                        <?=$item->model->brand->name;?> <?=$item->model->name;?>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <?php else: ?>
            <div class="block">
            </div>
            <?php endif; ?>
        </div>
    </div>
    
        

</div>
<div class="left-column">
<div class="request" id="request">
    <div class="title">Заявка</div>
    <div class="city-img"></div>
    <div class="model-preview"></div>
    <div class="body">
        <div class="form-row">
            <div class="label-single">Марка</div>
            <div class="form-ctrl">
                <select id="brand" name="brand">
                    <option value="0">Не выбрана</option>
                    <?php foreach ($brands as $brand): ?>
                    <option value="<?=$brand->id; ?>"><?=$brand->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="label-single">Модель</div>
            <div class="form-ctrl">
                <select id="model" name="model">
                    <option value="0">Не выбрана</option>
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="label-single">Комплектация</div>
            <div class="form-ctrl">
                <select id="complectation" name="complectation">
                    <option value="0">Не выбрана</option>
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="label-single">Цена</div>
            <div class="form-ctrl">
                <input disabled="disabled" maxlength="9" type="text" name="price" id="price" value="">
            </div>
        </div>
        <div class="form-row">
            <div class="label-single">ФИО</div>
            <div class="form-ctrl">
                <input maxlength="30" type="text" name="fio" value="">
            </div>
        </div>
        <div class="form-row">
            <div class="label-single">Контактный телефон</div>
            <div class="form-ctrl">
                <input type="text" name="phone" id="phone" value="">
            </div>
        </div>
        <div class="form-row">
            <div class="label-single">Возраст</div>
            <div class="form-ctrl">
                <input maxlength="2" type="text" name="age" id="age" value="">
            </div>
        </div>
        <div class="form-row">
            <div class="label-single">Регион прописки</div>
            <div class="form-ctrl">
                <select id="region" name="region">
                    <option value="0">Не выбран</option>
                    <?php foreach ($regions_list as $region): ?>
                    <option value="<?=$region->id; ?>"><?=$region->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-row first-payment-row">
            <div class="f-p">Первоначальный взнос: <span id="first-payment">0 руб.</span></div>            
            <input type="hidden" name="first-payment-value" value="">
            
            <div class="firstpay-progressbar">
                <span class="firstpay-progressbar-value">20%</span>
                <div id="slider-firstpay"></div>
            </div>
        </div>
        <div class="credit-calc">
            <a id="calc-slide" href="javascript:;">Расчитать Ваш ежемесячный платеж</a>
            <div id="credit-calc-container" style="display: none;">
                <div class="payment-year">
                    <div class="calc-info">Срок кредитования: <b>60 мес.</b></div>
                    <div class="progressbar">
                        <div id="slider-payment-year"></div>
                    </div>
                </div>
                <div id="calc-result">
                    <div class="descr">Ежемесячный платеж: <span class="result"><span></span></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="submitting">
		<p class="tradein">Если у Вас есть автомобиль, Вы можете сдать его в<br>качестве первоначального взноса по программе Trade-In</p>
        <div class="checkpoint _tradein">
            <span class="label js-label">У меня есть автомобиль на Trade-In</span>
            <span class="niceCheck">
                <input type="checkbox" name="dop" id="tradein" tabindex="0" value="">
            </span>
        </div>
        <div class="credit_but">
            <div class="button clear-input block-center">Отправить</div>
        </div>
    </div>
</div>
</div>
<div class="information">
    <div class="conditions">
        <div class="title">Кредитование</div>
        <ul>
            <li><span>Срок кредита от 6 месяцев до 7 лет</span></li>
            <li><span>Специальные программы автокредитования от 2,9% годовых</span></li>
            <li><span>Более 50 кредитных программ</span></li>
            <li><span>Кредит от 60 000 до 3 000 000 рублей</span></li>
            <li><span>Первоначальный взнос от 0% до 70% стоимости автомобиля</span></li>
			<li><span>Возможность досрочного погашения кредита без штрафных санкций</span></li>
            <li><span>Возможность сдать свое старое авто по программе Trade-in в качестве первоначального взноса</span></li>
			<li><span>КАСКО в подарок!</span></li>
        </ul>
    </div>
    <div class="requirements">
        <div class="title">Требования к клиенту</div>
        <ul>
            <li><span>Гражданство РФ</span></li>
            <li><span>Водительское удостоверение</span></li>
            <li><span>Возраст заемщика от 23 до 64 лет</span></li>
        </ul>
    </div>
<!--    <div class="calc">
        <div class="title">Расчет по кредиту</div>
        <ul>
            <li><span>Сумма кредита N руб.</span></li>
            <li><span>Срок кредитования N мес.</span></li>
        </ul>
    </div>
    <div class="payment-title">Ежемесячный платеж</div>
    <div class="payment">67 000 руб.</div> -->
</div>
<div class="brands">
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="2" class="brand-logo" style="background: url('/resources/img/chevrolet.png');"></div>
                    </div>  
                    <p class="brand text-center">Chevrolet</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="13" class="brand-logo" style="background: url('/resources/img/lada.png');"></div>
                    </div>  
                    <p class="brand text-center">Lada</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="27" class="brand-logo" style="background: url('/resources/img/uaz.png');"></div>
                    </div>  
                    <p class="brand text-center">УАЗ</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="3" class="brand-logo" style="background: url('/resources/img/citroen.png');"></div>
                    </div>  
                    <p class="brand text-center">Сitroen</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="6" class="brand-logo" style="background: url('/resources/img/ford.png');"></div>
                    </div>  
                    <p class="brand text-center">Ford</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="8" class="brand-logo" style="background: url('/resources/img/great_wall.png');"></div>
                    </div>  
                    <p class="brand text-center">Great Wall</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="10" class="brand-logo" style="background: url('/resources/img/hyundai.png');"></div>
                    </div>  
                    <p class="brand text-center">Hyundai</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="12" class="brand-logo" style="background: url('/resources/img/kia.png');"></div>
                    </div>  
                    <p class="brand text-center">Kia</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="15" class="brand-logo" style="background: url('/resources/img/mazda.png');"></div>
                    </div>  
                    <p class="brand text-center">Mazda</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="16" class="brand-logo" style="background: url('/resources/img/mitsubishi.png');"></div>
                    </div>  
                    <p class="brand text-center">Mitsubishi</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="17" class="brand-logo" style="background: url('/resources/img/nissan.png');"></div>
                    </div>  
                    <p class="brand text-center">Nissan</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="18" class="brand-logo" style="background: url('/resources/img/opel.png');"></div>
                    </div>  
                    <p class="brand text-center">Opel</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="20" class="brand-logo" style="background: url('/resources/img/renault.png');"></div>
                    </div>  
                    <p class="brand text-center">Renault</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="21" class="brand-logo" style="background: url('/resources/img/skoda.png');"></div>
                    </div>  
                    <p class="brand text-center">Skoda</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="24" class="brand-logo" style="background: url('/resources/img/toyota.png');"></div>
                    </div>  
                    <p class="brand text-center">Toyota</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="25" class="brand-logo" style="background: url('/resources/img/volkswagen.png');"></div>
                    </div>  
                    <p class="brand text-center">Volkswagen</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="14" class="brand-logo" style="background: url('/resources/img/lifan.png');"></div>
                    </div>  
                    <p class="brand text-center">Lifan</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="23" class="brand-logo" style="background: url('/resources/img/suzuki.png');"></div>
                    </div>  
                    <p class="brand text-center">Suzuki</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="19" class="brand-logo" style="background: url('/resources/img/peugeot.png');"></div>
                    </div>  
                    <p class="brand text-center">Peugeot</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="5" class="brand-logo" style="background: url('/resources/img/datsun.png');"></div>
                    </div>  
                    <p class="brand text-center">Datsun</p>
            </li>
<li class="item pull-left">
                    <div class="wrapper">
                        <div data-id="1" class="brand-logo" style="background: url('/resources/img/chery.png');"></div>
                    </div>  
                    <p class="brand text-center">Chery</p>
            </li>
</div>
<div class="requests">
    <?php echo \Yii::$app->view->renderFile('@app/views/site/fakerequest.php', [
        'status' => $status,
        'cars' => $cars,
        'regions' => $regions,
        'solutions' => $solutions,
        'new' => $new,
        'completed' => $completed
    ]); ?>
</div>
<img class="banki" src="/resources/img/banki.png"/>
<div class="page-header">
    <div class="title-line">
        <div class="item logo"></div>
        <div class="item address">
            <div class="city">
                г. Москва
            </div>
            <div class="extra">
                ул. Кировоградская, д. 11Б<br>
                пн-вс с 9:00 до 20:00
            </div>
        </div>
        <div class="item main-number">
            <div class="main-text">
                8 800 500 26 98
            </div>
            <div class="extra-text">
                по россии - бесплатно
            </div>
        </div>
        <div class="item extra-number">
            <div class="main-text">
                8 499 322 44 91
            </div>
            <div class="extra-text">
                отдел продаж
            </div>
        </div>
    </div>
    <div class="brands-line">
        <?php if (!empty($default_brand)): ?>
            <script>
                var default_brand_id = <?=$default_brand->id;?>;
            </script>
        <?php endif; ?>
        <?php foreach ($brands as $brand): ?>
            <div class="item pull-left">
                <a href="/<?=$brand->alias;?>" class="wrapper">
                    <div data-id="<?=$brand->id;?>" class="brand-logo" style="background: url('<?=$brand->img_path;?>');"></div>
                </a>  
                <p class="brand text-center"><?=$brand->name;?></p>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="main-banner">
        <?php if (!empty($models)): ?>
            <div class="title-name">
                <?=$default_brand->name;?>
            </div>
            <div class="box-shadow">
                <div class="models-line demonstration-list">
                    <?php echo \Yii::$app->view->renderFile('@app/views/site/models_line.php', ['models' => $models]); ?>
                </div>
            </div>
        <?php elseif (!empty($complectations)): ?>
            <script>
                var default_brand_id = <?=$default_brand->id;?>;
                var default_model_id = <?=$default_model->id;?>;
            </script>
            <div class="title-name">
                <?=$default_brand->name;?> <?=$default_model->name;?>
            </div>

            <div class="box-shadow">
                <div class="complectations-line demonstration-list">
                    <?php echo \Yii::$app->view->renderFile('@app/views/site/complectations_line.php', ['complectations' => $complectations, 'default_model' => $default_model]); ?>     
                </div>
            </div>
        <?php endif; ?>
            <div class="box-shadow">
                <a href="/nissan/x_trail">
                    <img class="img" src="/resources/img/banner.jpg" />
                </a>
            </div>
        </div>
        <div class="bottom-title">кредитование</div>
    </div>
</div>
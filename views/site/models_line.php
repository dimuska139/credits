<?php if (!empty($models)): ?>
    <?php 
        $brand = app\models\Brand::findOne($models[0]->brand_id);
    ?>
    <?php foreach ($models as $model): ?>
    <a href="/<?=$brand->alias;?>/<?=$model->alias;?>" class="item">
        <div class="img" style="background-image: url('<?=$model->img_preview_path;?>')">

        </div>
        <div class="name">
            <?=$model->name; ?>
        </div>
        <div class="min-price">
            <?php
                $minPrice = app\models\Complectation::find()->where("status=1 AND model_id=".$model->id)->min('price');
            ?>
            от <?=number_format(($minPrice), 0, '.', ' '); ?> руб.
        </div>
    </a> 
    <?php endforeach; ?>
<?php endif; ?>
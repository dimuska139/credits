<div class="request_list">
    <div class="title">
        <div class="status"><?=$status;?></div>
        <div class="text">Заявки на рассмотрении</div>
    </div>
    <div class="table">
        <div class="head">
            <div class="number">заявка</div>
            <div class="car">автомобиль</div>
            <div class="sum">сумма</div>
            <div class="region">регион</div>
        </div>
        <div class="body">
            <?php foreach ($new as $num => $request): ?>
                <div class="line<?=$request->id%2==0?' odd':'';?>" data-id="<?=$request->id; ?>">
                    <div class="number">
                        <?=$request->number; ?>
                    </div>
                    <div class="car">
                        <?php if ($request->fake): ?>
                            <?=$cars[$request->car_id]; ?>
                        <?php else: ?>
                            <?php
                                $complectation = app\models\Complectation::findOne($request->complectation_id);
                                $model = app\models\Model::findOne($complectation->model_id);
                                $brand = app\models\Brand::findOne($model->brand_id);
                            ?>
                            <?=$brand->name." ".$model->name;?>
                        <?php endif; ?>
                    </div>
                    <div class="sum">
                        <?php if ($request->fake): ?>
                            <?=number_format(($request->price*1000), 0, '.', ' '); ?> р.
                        <?php else: ?>
                            <?=number_format(($request->price), 0, '.', ' '); ?> р.
                        <?php endif; ?>
                    </div>
                    <div class="region">
                        <?=$regions[$request->region_id]; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<div class="solution_list">
    <div class="title">
        <div class="status"><?=$status;?></div>
        <div class="text">Решения по кредитам</div>
    </div>
    <div class="table">
        <div class="head">
            <div class="number">заявка</div>
            <div class="car">автомобиль</div>
            <div class="solution">решение банка</div>
        </div>
        <div class="body">
            <?php foreach ($completed as $num => $request): ?>
                <div class="line<?=$request->id%2==0?' odd':'';?>" data-id="<?=$request->id; ?>">
                    <div class="number">
                        <?=$request->number; ?>
                    </div>
                    <div class="car">
                        <?php if ($request->fake): ?>
                            <?=$cars[$request->car_id]; ?>
                        <?php else: ?>
                            <?php
                                $complectation = app\models\Complectation::findOne($request->complectation_id);
                                $model = app\models\Model::findOne($complectation->model_id);
                                $brand = app\models\Brand::findOne($model->brand_id);
                            ?>
                            <?=$brand->name." ".$model->name;?>
                        <?php endif; ?>
                    </div>
                    <div class="solution">
                        <?php if (!empty($request->solution_id)): ?>
                            <?php if ($request->solution_id==1): ?>
                                <div class="success">одобрено</div>
                            <?php else: ?>
                                <span class="error">
                                    <?=$solutions[$request->solution_id]; ?>
                                </span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php echo \Yii::$app->view->renderFile('@app/views/site/header.php', ['models' => $models, 'brands' => $brands, 'default_brand' => $default_brand]); ?>
<div class="page">
    <?php echo \Yii::$app->view->renderFile('@app/views/site/steps.php'); ?>
    <div class="content">
        <?php echo \Yii::$app->view->renderFile('@app/views/site/addrequestform.php',[
                'brands' => $brands,
                'regions_list' => $regions_list,
                'status' => $status,
                'cars' => $cars,
                'regions' => $regions,
                'solutions' => $solutions,
                'new' => $new,
                'completed' => $completed
            ]);
        ?>
    </div>
</div>


<div class="steps">
    <div class="item paper"></div>
    <div class="item documents">
        <div class="arrow"></div>
    </div>
    <div class="item phone">
        <div class="arrow"></div>
    </div>
    <div class="item car">
        <div class="arrow"></div>
    </div>
</div>
<div class="steps-description">
    <div class="item">
        <div class="title">заполните заявку</div>
        <div class="text">заполнение заявки онлайн<br>займет у Вас менее 2 минут</div>
    </div>
    <div class="item">
        <div class="title">рассмотрение</div>
        <div class="text">заявка будет отправлена<br>23 банкам-партнерам</div>
    </div>
    <div class="item">
        <div class="title">оставайтесь на связи</div>
        <div class="text">среднее время рассмотрения<br>заявки до 20 минут</div>
    </div>
    <div class="item">
        <div class="title">оформление</div>
        <div class="text">получите автомобиль, остаются<br>лишь мелкие формальности</div>
    </div>
</div>
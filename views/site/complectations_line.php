<?php if (!empty($complectations)): ?>
    <?php foreach ($complectations as $complectation): ?>
    <div class="item">
        <div class="col model-name">
            <?=$default_model->name; ?>
        </div>
        <div class="col complectation-name">
            <?=$complectation->clear_name; ?>
        </div>
        <div class="col price">
            <?=number_format(($complectation->price), 0, '.', ' '); ?> руб.
        </div>
        <div class="col count-credit">
            <div data-id="<?=$complectation->id; ?>" class="button">рассчитать кредит</div>
        </div>
        <div class="description"><?=$complectation->description; ?></div>
    </div> 
    <?php endforeach; ?>
<?php endif; ?>
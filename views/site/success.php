<div class="body">
    <div class="result-text">
        Просим Вас не отправлять заявки с других сайтов<br>
        во избежание повторных запросов в банки.<br>
        Это может негативно сказаться на решении банка.<br>
        Спасибо за понимание!
    </div>
    <div class="result-img"></div>
</div>
<div class="submitting">
    <div class="manager-answer">
        Заявка успешно отправлена!<br>В ближайшее время наш менеджер свяжется с Вами.
    </div>
</div>
<div class="counters">
    <div class="new item">
        <a class="text<?=($type=="new"?" active":"");?>" href="/admin/?type=new">Новые </a><span class="amount">(<span id="new_requests_amount"><?=$requests_new_amount;?></span>)</span>
    </div>
    <div class="all item">
        <a class="text<?=($type=="all"?" active":"");?>" href="/admin/?type=all">Все заявки </a></span><span class="amount">(<span id="all_requests_amount"><?=$requests_all_amount;?></span>)</span>
    </div>
</div>
<script>
    var type = "<?=$type;?>";
    var grades = [];
    <?php foreach ($grades as $id => $grade): ?>
        grades[<?=$id;?>] = "<?=$grade;?>";
    <?php endforeach; ?>
</script>
<div class="list">
    <?php if (empty($requests)): ?>
        <h1>Нет заявок</h1>
    <?php else: ?>
        <?php foreach ($requests as $request): ?>
        <div class="item-<?=$request->id;?> item<?=($request->completed==0?" new":"");?>">
            <div class="date">
                <?=(new \DateTime($request->created_at))->format("m-d H:i");?>
            </div>
            <?php
                $complectation = app\models\Complectation::findOne($request->complectation_id);
                $model = app\models\Model::findOne($complectation->model_id);
                $brand = app\models\Brand::findOne($model->brand_id);
            ?>
            <div class="col col-1">
                <div class="title">Автомобиль</div>
                <div class="line">
                    <div class="label">Марка:</div>
                    <div class="value">
                        <?=$brand->name; ?>
                    </div>
                </div>
                <div class="line">
                    <div class="label">Модель:</div>
                    <div class="value">
                        <?=$model->name; ?>
                    </div>
                </div>
                <div class="line">
                    <div class="label">Компл.:</div>
                    <div class="value">
                        <?=$complectation->name; ?>
                    </div>
                </div>
                <div class="line">
                    <div class="label">Цена:</div>
                    <div class="value">
                        <?=number_format(($request->price), 0, '.', ' '); ?> р.
                    </div>
                </div>
            </div>
            <div class="col col-2">
                <div class="title">Кредит</div>
                <div class="line">
                    <div class="label">Взнос:</div>
                    <div class="value">
                        <?=number_format(($request->first_payment), 0, '.', ' '); ?> р.
                    </div>
                </div>
                <div class="line">
                    <div class="label">Сумма:</div>
                    <div class="value">
                        <?=number_format(($request->price), 0, '.', ' '); ?> р.
                    </div>
                </div>
                <div class="line">
                    <div class="label">Срок:</div>
                    <div class="value">
                        <?php
                            $years = $request->years;
                            $years_arr = explode('.', $years);
                            if ($years_arr[count($years_arr)-1]=='0')
                                unset($years_arr[count($years_arr)-1]);
                            $years = implode(".", $years_arr);
                        ?>
                        <?=$years; ?> месяцев
                    </div>
                </div>
                <div class="line">
                    <div class="label">Платеж:</div>
                    <div class="value">
                        <?=number_format(($request->every_month_payment), 0, '.', ' '); ?> р.
                    </div>
                </div>
            </div>
            <div class="col col-3">
                <div class="title">Контакты</div>
                <div class="line">
                    <div class="label">ФИО:</div>
                    <div class="value">
                        <?=$request->fio; ?>
                    </div>
                </div>
                <div class="line">
                    <div class="label">Возраст:</div>
                    <div class="value">
                        <?=$request->age; ?>
                    </div>
                </div>
                <div class="line">
                    <div class="label">Регистрация:</div>
                    <div class="value">
                        <?=$regions[$request->region_id]; ?>
                    </div>
                </div>
                <div class="line">
                    <div class="label">Телефон:</div>
                    <div class="value">
                        <?=$request->phone; ?>
                    </div>
                </div>
            </div>
            <div class="col col-4">
                <?php if ($request->completed==0): ?>
                <select class="grade">
                    <option value="0">Выставите оценку</option>
                <?php foreach ($grades as $id => $grade): ?>
                    <option value="<?=$id;?>"><?=$grade;?></option>
                <?php endforeach; ?>
                </select>
                <div class="save" data-id="<?=$request->id;?>">
                    Сохранить
                </div>
                <?php else: ?>
                    Оценка: <?=$grades[$request->grade_id];?>
                <?php endif; ?>
            </div>
        </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
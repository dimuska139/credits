<form class="auth-form" action="/admin/" method="post">
    <div class="line">
        <div class="label">Логин</div>
        <div class="form-control">
            <input type="text" name="login">
        </div>
    </div>
    <div class="line">
        <div class="label">Пароль</div>
        <div class="form-control">
            <input type="password" name="password">
        </div>
    </div>
    <?php if (!empty($error_message)): ?>
    <div class="error"><?=$error_message;?></div>
    <?php endif; ?>
    <div class="line">
        <div class="label"></div>
        <div class="form-control">
            <input type="submit" value="Войти">
        </div>
    </div>
</form>
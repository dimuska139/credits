<?php
    use yii\helpers\Html;
    use app\assets\AppAsset;
    AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width"/>
        <meta name="viewport" content="target-densitydpi=high-dpi" />
        <meta http-equiv="Content-Language" content="ru"/>
		<meta name="format-detection" content="telephone=no">
		<meta http-equiv="x-rim-auto-match" content="none">
        <meta name="viewport" content="width=1200">
        <?php if (!empty($this->params['off_format_detection'])):?>
            <meta name="format-detection" content="telephone=no" />
        <?php endif; ?>
        <?= Html::csrfMetaTags() ?>
        <title>Группа компаний "Автостар"</title>
        <link rel="icon" type="image/png" href="/resources/img/favicon.png" />
        <link rel="apple-touch-icon" href="/resources/img/ios.png" />
            
        <?php $this->head() ?>
    </head>

    <body>
        <?php $this->beginBody() ?>
        <div class="center_wrap">
            <div class="content">
                <?= $content ?>
            </div>
        </div>
        <?php $this->endBody() ?>
        <?php echo \Yii::$app->view->renderFile('@app/views/site/yandex_metrika.php'); ?>	
    </body>
</html>
<?php $this->endPage() ?>
